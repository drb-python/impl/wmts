===================
Data Request Broker
===================
---------------------------------
Web Map Service driver for DRB
---------------------------------
Release v\ |version|.


.. image:: https://pepy.tech/badge/drb-driver-wmts/month
    :target: https://pepy.tech/project/drb-driver-wmts
    :alt: Requests Downloads Per Month Badge

.. image:: https://img.shields.io/pypi/l/drb-driver-wmts.svg
    :target: https://pypi.org/project/drb-driver-wmts/
    :alt: License Badge

.. image:: https://img.shields.io/pypi/wheel/drb-driver-wmts.svg
    :target: https://pypi.org/project/drb-driver-wmts/
    :alt: Wheel Support Badge

.. image:: https://img.shields.io/pypi/pyversions/drb-driver-wmts.svg
    :target: https://pypi.org/project/drb-driver-wmts/
    :alt: Python Version Support Badge

-------------------

This drb-driver-wmts module implements access to Web Map Tile Service (WMTS).

User Guide
==========

.. toctree::
   :maxdepth: 2

   user/install


.. toctree::

   dev/api

Example
=======

.. toctree::
   :maxdepth: 2

   dev/example



