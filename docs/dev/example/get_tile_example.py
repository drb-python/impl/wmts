from drb.drivers.wmts import WmtsServiceNode, WmtsGetTilePredicate

url_wmts = 'https+wmts://geoserver.swarm.ops.internal.gael.fr/geoserver/demo/wmts?'


service_wmts = WmtsServiceNode(url_wmts)

list_cap = [e.name for e in service_wmts]

print('----------------------------------------')
print('list_cap')

print(list_cap)

for child in service_wmts:
    print(child)
    print(child.name)

# =>  <drb_impl_wXs.wXs_node.WXSNodeOperation object at 0x7f5403865d90>
# =>  GetCapabilities
# =>  <drb_impl_wmts.wmts_nodes.WmtsNodeOperationGetTile object at 0x7f54047b4700>
# =>  GetTile
# =>  <drb_impl_wmts.wmts_nodes.WmtsNodeOperationGetFeatureInfo object at 0x7f54047b4460>
# =>  GetFeatureInfo
# =>  <drb_impl_xml.xml_node.XmlNode object at 0x7f5402914c70>
# =>  Contents


get_map = service_wmts['GetTile']

dict_request = {'layer': 'ORTHOIMAGERY.ORTHOPHOTOS', 'format': 'image/jpeg',
                'TILEMATRIXSET': 'PM', 'TILEMATRIX': 14, 'TILECOL': 8180,
                'TILEROW': '5905', 'style': 'normal'}

tile_res = service_wmts['GetTile'][dict_request]

print('----------------------------------------')
print('GetTile : with parameter return image')
print(tile_res)
# =>  <drb_impl_image.image_node_factory.DrbImageBaseNode object at 0x7fc2cb23efa0>


predicate = WmtsGetTilePredicate(layer='ORTHOIMAGERY.ORTHOPHOTOS',
                                 tile_matrix_set='PM',
                                 tile_matrix=14, tile_col=8180, tile_row=5905,
                                 style='normal')

print('----------------------------------------')
print('GetTile : with parameter return image')
print(tile_res)
# =>  <drb_impl_image.image_node_factory.DrbImageBaseNode object at 0x7f54047b4970>