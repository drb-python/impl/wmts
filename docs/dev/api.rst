.. _api:

Reference API
=============

WmtsServiceNode
-----------------

Represents the WMTS service. This node has no attribute and
has as children WmtsOperationNode like GetTile.
Others children give information about the service like layers, and are XmlNode.
Those children are filled in by the information returned from
the service's GetCapabilities request.

.. autoclass:: drb.drivers.wmts.WmtsServiceNode
    :members:


WmtsOperationNode
-----------------

Represents an operation than can mde on the service.
For WMTS service, the mandatory operation are GetTile, GetCapabilities, and
GetFeatureInfo.
Optional operations may be provided by the service
and indicated in the possibilities thereof.
Those optional operations are also represented as WmtsOperationNode.

For perform an operation (mandatory or optional), you can use the operator '[]' with a dict that contains
the parameters of the request.

Example:
```
dict_request = {'layers': 'mgrs_region', 'format': 'image/png', 'height': 256, 'width': 256, 'crs': 'EPSG:3857', 'bbox': '7514065.628545968,7514065.628545967,10018754.171394622,10018754.171394628'}


get_map = service_wmts['GetTile'][dict_request]
```

For mandatory operations GetTile and GetFeatureInfo you can
alternatively use Predicate WmtsGetTilePredicate and WmtsGetFeatureInfoPredicate.

Specific class define WXSNodeOperationGetTile and WXSNodeOperationGetFeatureInfo
for accept respectively WmtsGetTilePredicate WmtsGetFeatureInfoPredicate.

Example:
```
predicate = WmtsGetTilePredicate(layer='ORTHOIMAGERY.ORTHOPHOTOS',
                                 tile_matrix_set='PM',
                                 tile_matrix=14, tile_col=8180, tile_row=5905,
                                 style='normal')

get_map = service_wmts['GetTile'][predicate]
```

.. autoclass:: drb.drivers.wmts.WmtsNodeOperationGetTile
    :members:
