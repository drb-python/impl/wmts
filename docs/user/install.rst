.. _install:

Installation of WMTS driver
====================================
Installing ``drb-driver-wmts`` with execute the following in a terminal:

.. code-block::

    pip install drb-driver-wmts
