from drb.exceptions.core import DrbException


class WmtsRequestException(DrbException):
    pass
